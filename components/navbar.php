<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Jimenez YNS Dev Exercises</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="/yns-dev-exercises/practice/5-3/5-3.php">Home</a></li>
            <li><a href="/yns-dev-exercises/practice/5-2/5-2.php">Calendar</a></li>
            <li><a href="/yns-dev-exercises/practice/5-1/5-1.php">Quiz</a></li>
            <li><a href="/yns-dev-exercises/sql/3-5.login.php">Users</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="/yns-dev-exercises/">HTML & PHP
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/yns-dev-exercises/htmlphp/1-1.php">Activity 1-1</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-2.php">Activity 1-2</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-3.php">Activity 1-3</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-4.php">Activity 1-4</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-5.php">Activity 1-5</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-6.php">Activity 1-6</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-7.php">Activity 1-7</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-8.php">Activity 1-8</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-9.php">Activity 1-9</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-10.php">Activity 1-10</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-11.php">Activity 1-11</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-12.php">Activity 1-12</a></li>
                    <li><a href="/yns-dev-exercises/htmlphp/1-1.php">Activity 1-13</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="/yns-dev-exercises/">Javascript
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/yns-dev-exercises/javascript/2-1.html">Activity 2-1</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-2.html">Activity 2-2</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-3.html">Activity 2-3</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-4.html">Activity 2-4</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-5.html">Activity 2-5</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-6.html">Activity 2-6</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-7.html">Activity 2-7</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-8.html">Activity 2-8</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-9.html">Activity 2-9</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-10.html">Activity 2-10</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-11.html">Activity 2-11</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-12.html">Activity 2-12</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-13.html">Activity 2-13</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-14.html">Activity 2-14</a></li>
                    <li><a href="/yns-dev-exercises/javascript/2-15.html">Activity 2-15</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>