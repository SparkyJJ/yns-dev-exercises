-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2019 at 05:08 AM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `3-5`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `first_name` varchar(64) NOT NULL,
  `middle_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) NOT NULL,
  `age` int(8) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `contact` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `picture_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `age`, `gender`, `address`, `birthdate`, `contact`, `email`, `username`, `password`, `picture_id`) VALUES
(1, 'Jimmy', 'Zulueta', 'Jimenez', 21, 'male', 'Mandaluyong', '2019-09-11', '0927-574-9921', 'jimmyjakejimenez@gmail.com', 'Sparky', '$2y$10$Ywqxjg7.bT8XFVzTBVJuIOFp61pnmPhvpHYUxJij/zKXqeQo.weRC', 'overwatch.png'),
(2, 'Jimmy', 'Jake', 'Jimenez', 21, 'male', 'Mandaluyong', '0809-09-09', '0923-231-2319', 'jimmyjakejimenez@gmail.com', 'Sparky14', '$2y$10$RoP6uUQO0lsIqZninuBjy.kZ4AdqM4fTMI52ZpMOIGC7klGpkwQiW', NULL),
(3, 'Jimmy', 'Jake', 'Jimenez', 21, 'male', 'Mandaluyong', '0809-09-09', '0923-231-2319', 'jimmyjakejimenez@gmail.com', 'Sparky15', '$2y$10$T0ZvvjO5yRZgtS0PSfQdluBER8hozYuuLgBx91Te6CL/kAUlRP0vm', NULL),
(4, 'Sammy', 'Navarro', 'Damao', 24, 'male', 'Mandaluyong', '1998-02-22', '0923-231-2319', 'sam@gmail.com', 'Thevenize', '$2y$10$qJt91RzeXyo0OiofD9oF2Oo7UfdfOjy.b6QhX4oVaoSBohJIkxaDW', '2019-09-27-1569551915.jpg'),
(5, 'Michelle Anne', 'B', 'Sayson', 26, 'male', 'Makati', '1998-11-11', '0923-231-2319', 'mich@gmail.com', 'Tristesse', '$2y$10$S.kkin8vdmPNl63t8SI4seg45V1VFyIwf3kP9iTefUnBrVYRl49N.', '2019-09-27-1569552169.png'),
(6, 'Keith', 'Andrew', 'Pacio', 18, 'male', 'Mandaluyong', '1998-02-22', '0912-523-3262', 'keith@gmail.com', 'BKStar', '$2y$10$kh3pJLRBWQ2oQ3idWrNYKeKqZITBXZBaYLRCk6mWpb4E9giUxTSmG', '2019-09-27-1569552817.jpg'),
(7, 'Keith', 'Andrew', 'Pacio', 18, 'male', 'Mandaluyong', '1998-02-22', '0912-523-3262', 'keith@gmail.com', 'BKStar3', '$2y$10$/JeF5aTa4ehQyPaoBTOiBeT9lTjq5W6DoibR91mklvfcsHr5UGvPi', NULL),
(8, 'Lauro', 'G', 'Bernardo', 21, 'male', 'Manila', '1997-11-11', '0923-231-2319', 'lauro@gmail.com', 'Entropy', '$2y$10$5B/SAb0gGM.omEiaTZzftOTxWVhayPlxb1u8oEI22XBTSleNK0lCC', NULL),
(9, 'Jefferson', '', 'Vasquez', 29, 'male', 'Muntinlupa', '1997-02-04', '0923-231-2319', 'jeff@gmail.com', 'Entropy2', '$2y$10$tB5ac8TM9bzd.6T3hLBlbu90J6cuVD8zA2WeStzlqXaOcv4e5X1GS', NULL),
(10, 'Chris', '', 'Puckett', 25, 'male', 'Los Angeles', '1998-05-02', '0912-523-3262', 'puckett@ow.com', 'Puckett', '$2y$10$osqhsctUa4k5vLPHP7oif.oaGP457H6hbAvXJoUdd4roUyiszfdDO', NULL),
(11, 'Bap', '', 'Tiste', 19, 'male', 'France', '1976-02-04', '0923-244-2421', 'bap@tiste.com', 'Baptiste', '$2y$10$WyvUyZGMXkHuopShWRaIBuB3wjIBBl.4vd8P.rPRucykuZf5s3bJS', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
