<?php
require_once 'connect.php';
define('CSV_FILE', '1-13.users.csv');
session_start();
if (isset($_SESSION['access'])) {
    header('Location: 3-5.php');
    exit();
}

if (isset($_POST['register'])) {
    header('Location: 3-5.register.php');
} elseif (isset($_POST['login'])) {
    $username = mysqli_escape_string($connect, $_POST['username']);
    $password = mysqli_escape_string($connect, $_POST['password']);
    if (empty($username) || empty($password)) {
        $message = 'Please fill up the login fields.';
    } elseif (preg_match("/\s/", ($username . $password))) {
        $message = 'Whitespace is not allowed for username/password.';
    } elseif (strlen($password) < 8) { //Validates string length of password
        $message = 'Password should be at least 8 characters';
    } else {

        $query = 'SELECT username, password FROM users WHERE username = ?';

        $stmt = $connect->prepare($query);
        $stmt->bind_param(
            "s",
            $username
        );
        $stmt->execute();
        $queryResult = $stmt->get_result();
        $stmt->fetch();
        $stmt->close();
        if (!($queryResult->num_rows > 0)) {
            $message = 'Invalid username/password.';
        } else {
            if ($data = $queryResult->fetch_assoc()) {
                $hashedPwdCheck = password_verify($password, $data['password']);
                if (!$hashedPwdCheck) {
                    $message = 'Invalid username/password.';
                } else {
                    if (session_status() == PHP_SESSION_NONE) {
                        session_start();
                    }
                    $_SESSION['username'] = $username;
                    $_SESSION['password'] = $password;
                    $_SESSION['access'] = true;

                    header('Location: 3-5.php');
                    exit();
                }
            }
        }
    }
}

?>
<html>

<head>
    <title>SQL 3-5</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/yns-dev-exercises/assets/bootstrap.min.css">
    <script src="/yns-dev-exercises/assets/jquery.min.js"></script>
    <script src="/yns-dev-exercises/assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../components/navbar.php';
    ?>
    <div class="container" style="margin-top: 75px;">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h2>Information System</h2>
                <?php
                if (isset($message)) {
                    echo "<h4 style='color: red'>$message</h3>";
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <form action="" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label for="username" class="control-label col-sm-3"><b>Username: </b></label>
                        <div class="col-sm-8">
                            <input type="text" placeholder="Enter Username" name="username" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-3"><b>Password: </b></label>
                        <div class="col-sm-8">
                            <input type="password" placeholder="Enter Password" name="password" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" name="login" class="btn btn-primary col-sm-12">Login</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <a href="3-5.register.php">
                                <button type="button" name="register" class="btn btn-success col-sm-12">Register</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>