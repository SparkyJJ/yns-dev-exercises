<?php
require_once 'pdo_connect.php';
// require_once 'connect.php';
define('IMAGE_DIRECTORY', 'profile_picture/');
define('DEFAULT_IMAGE', 'default.png');
define('NUM_PER_PAGE', 10);

session_start();
if (!$_SESSION['access']) {
    header("Location: 3-5.login.php?access=invalid");
} else {
    try {

        $username = $_SESSION['username'];
        $password = $_SESSION['password'];

        $query = 'SELECT 
        id,
        first_name, 
        middle_name, 
        last_name, 
        age, 
        gender, 
        address, 
        birthdate, 
        contact, 
        email, 
        username, 
        picture_id
        FROM 
        users';

        //Retrieve user info
        $queryUser = $query . " WHERE username = ?";
        $stmt = $connect->prepare($queryUser);
        $stmt->execute([$username]);
        $userInfo = $stmt->fetch();
        $stmt = null;

        $queryCount = "SELECT COUNT(DISTINCT username) FROM users";
        $stmt = $connect->prepare($queryCount);
        $stmt->execute();
        $queryResult = $stmt->fetch(PDO::FETCH_COLUMN);
        $stmt = null;
        $totalPage = ceil($queryResult / NUM_PER_PAGE);

        //Retrieve rows from database based on pagination
        $page = 1;
        if (isset($_GET['page'])) {
            $getPage = $_GET['page'];
            if ($getPage > 0 && $getPage <= $totalPage) {
                $page = $getPage;
            } else {
                header('Location: ?page=1');
            }
        }
        $offset = ($page - 1) * NUM_PER_PAGE;

        $queryDisplay = $query . " LIMIT " . $offset . ", " . NUM_PER_PAGE;
        $stmt = $connect->prepare($queryDisplay);
        $stmt->execute();
        $display = $stmt->fetchAll();
    } catch (Exception $e) {
        $message = "Error: Problem with database connection";
    }
}

if (isset($_POST['logout'])) {
    unset($_SESSION['username']);
    unset($_SESSION['password']);
    unset($_SESSION['access']);
    session_destroy();
    header("Location: 3-5.login.php");
    exit();
}
?>
<html>

<head>
    <title>SQL 3-5</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/yns-dev-exercises/assets/bootstrap.min.css">
    <script src="/yns-dev-exercises/assets/jquery.min.js"></script>
    <script src="/yns-dev-exercises/assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../components/navbar.php';
    ?>
    <div class="container-fluid" style="margin-top: 25px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 text-center">
                    <h2>Welcome <?= $userInfo['first_name'] . " " . $userInfo['last_name'] . "!" ?></h2>
                    <?php
                    if (isset($message)) {
                        echo "<h4 style='color: red'>$message</h3>";
                    }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1 col-sm-offset-2">
                    <?php
                    echo "<img src='";
                    if (isset($userInfo['picture_id']) && $userInfo['picture_id'] !== "") {
                        echo IMAGE_DIRECTORY . $userInfo['picture_id'];
                    } else {
                        echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                    }
                    echo "' style='max-width: 75px'>";
                    ?>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <?= "Age: " . $userInfo['age'] ?>
                        </div>
                        <div class="col-sm-6">
                            <?= "Gender: " . ucfirst($userInfo['gender']) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= "Contact: " . $userInfo['contact'] ?>
                        </div>
                        <div class="col-sm-6">
                            <?= "Email: " . $userInfo['email'] ?>
                        </div>
                        <div class="col-sm-6">
                            <?= "Address: " . $userInfo['address'] ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1">
                    <form method="POST">
                        <button type='submit' name='logout' class="btn btn-danger">Logout</button>
                    </form>
                </div>
            </div>
            <br>
            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Address</th>
                        <th>Birth Date</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($display as $data) {
                            echo "<tr>";
                            echo "<td>" . $data['id'] . "</td>";
                            echo "<td>";
                            echo "<img src='";
                            if (isset($data['picture_id']) && $data['picture_id'] !== "") {
                                echo IMAGE_DIRECTORY . $data['picture_id'];
                            } else {
                                echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                            }
                            echo "' style='max-width: 50px'>";
                            echo "</td>";
                            echo "<td>" . $data['username'] . "</td>";
                            echo "<td>" . $data['first_name'] . "</td>";
                            echo "<td>" . $data['middle_name'] . "</td>";
                            echo "<td>" . $data['last_name'] . "</td>";
                            echo "<td>" . $data['age'] . "</td>";
                            echo "<td>" . $data['gender'] . "</td>";
                            echo "<td>" . $data['address'] . "</td>";
                            echo "<td>" . $data['birthdate'] . "</td>";
                            echo "<td>" . $data['contact'] . "</td>";
                            echo "<td>" . $data['email'] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row text-center">
                <ul class="pagination">
                    <?php
                    if ($page > 1) {
                        echo "<li><a href='?page=" . ($page - 1) . "'>PREVIOUS</a></li>";
                    } else {
                        echo "<li class='disabled'><a>PREV</a></li>";
                    }

                    for ($i = 1; $i <= $totalPage; $i++) {
                        if ($i != $page) {
                            echo "<li><a href='?page=$i'>$i</a></li>";
                        } else {
                            echo "<li class='disabled'><a>$i</a></li>";
                        }
                    }

                    if ($page < $totalPage) {
                        echo "<li><a href='?page=" . ($page + 1) . "'>NEXT</a></li>";
                    } else {
                        echo "<li class='disabled'><a>NEXT</a></li>";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>