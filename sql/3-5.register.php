<?php
require_once 'pdo_connect.php';
define('IMAGE_DIRECTORY', 'profile_picture/');

$message = null;
$messageColor = null;
if (isset($_POST['submit'])) {
    try {
        extract($_POST);
        $isValid = true;
        foreach ($user as $key => $value) {
            if (is_string($value)) {
                $$key = trim($value);
            } else {
                $$key = $value;
            }

            if (empty($value) && $key != "mname") { //Validate empty fields.
                echo $value;
                $isValid = false;
                $message = 'Please complete the information form.';
                break;
            } elseif ($key == 'uid' || $key == 'pass') { //Prevents whitespaces in username/password
                if (preg_match("/\s/", $value)) {
                    $isValid = false;
                    $message = 'Whitespace is not allowed for username/password.';
                    break;
                }

                if ($key == 'uid') {
                    $query = 'SELECT 1 FROM users WHERE username = ?';
                    $stmt = $connect->prepare($query);
                    $stmt->execute([$value]);
                    $queryResult = $stmt->fetch(PDO::FETCH_COLUMN);
                    $stmt = null;

                    if ($queryResult === 1) {
                        $isValid = false;
                        $message = 'Username already taken.';
                        break;
                    }
                }

                if ($key == 'pass' && strlen($value) < 8) { //Validates string length of password
                    $isValid = false;
                    $message = 'Password should be at least 8 characters';
                    break;
                }
            } elseif ($key === 'fname' || $key === 'mname' || $key === 'lname') { //Characters only in names
                if (!preg_match("/^[a-zA-Z ]*$/", $value)) {
                    $isValid = false;
                    $message = 'Invalid character in name.';
                    break;
                }
            } elseif ($key == 'age') { //Characters only in names
                if (!preg_match("/^[0-9]+$/", $value)) {
                    $isValid = false;
                    $message = 'Numeric characters only for age.';
                    break;
                }
            } elseif ($key == 'birthdate') { //Validates birthday
                if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $value)) {
                    $isValid = false;
                    $message = 'Invalid birth date.';
                    break;
                }
            } elseif ($key == 'contact') { //Validates contact number format
                if (!preg_match("/^\d{4}-\d{3}-\d{4}$/", $value)) {
                    $isValid = false;
                    $message = 'Invalid contact number format.';
                    break;
                }
            } elseif ($key == 'email') { //Validates email through regex
                if (!preg_match("/^[a-zA-Z0-9]+\@[a-zA-Z0-9]+\.[a-z]{2,3}$/", $value)) {
                    $isValid = false;
                    $message = 'Invalid email format.';
                    break;
                }
            }
        }

        if ($user['pass'] !== $user['cpass']) { //Checks if password and confirm password is the same
            $isValid = false;
            $message = 'Password does not match confirm password.';
        } elseif (!empty($_FILES['picture']['name'])) { //Checks if an image is uploaded
            $imageFileType = strtolower(pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION));
            $filename = date('Y-m-d') . '-' . round(microtime(true)) . '.' . $imageFileType;
            $targetFile = IMAGE_DIRECTORY . $filename;
            $check = getimagesize($_FILES['picture']['tmp_name']);
            if ( //Image related validation
                $check == false || file_exists($targetFile)
                || $_FILES['picture']['size'] > 500000
                || ($imageFileType != 'jpg' && $imageFileType != 'png'
                    && $imageFileType != 'jpeg'
                    && $imageFileType != 'gif')
            ) {
                $message = 'Invalid image upload.';
                $isValid = false;
            } elseif (!move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile)) { //Uploads file to directory
                $message = 'Error uploading your image.';
                $isValid = false;
            } else {
                $user['pictureID'] = $filename;
            }
        } else {
            $user['pictureID'] = null;
        }

        if (!$isValid) {
            $messageColor = 'red';
        } else {
            $user['pass'] = password_hash($user['pass'], PASSWORD_DEFAULT);
            unset($user['cpass']);
            extract($user);
            try {


                $query = 'INSERT INTO users
                (
                    first_name,
                    middle_name,
                    last_name,
                    age,
                    gender,
                    address,
                    birthdate,
                    contact,
                    email,
                    username,
                    password,
                    picture_id
                ) 
                VALUES (
                    ?,?,?,?,
                    ?,?,?,?,
                    ?,?,?,?
                )';

                $connect->beginTransaction();
                $stmt = $connect->prepare($query);
                $stmt->execute(
                    [
                        $fname,
                        $mname,
                        $lname,
                        $age,
                        $gender,
                        $address,
                        $birthdate,
                        $contact,
                        $email,
                        $uid,
                        $pass,
                        $pictureID
                    ]
                );
                $stmt = null;
                $connect->commit();
                unset($user);
                $message = 'Submit successful!';
                $messageColor = 'green';
            } catch (Exception $e) {
                $connect->rollBack();
                throw $e;
            }
        }
    } catch (Exception $e) {
        exit('Error: Problem with database query');
    }
}
?>

<html>

<head>
    <title>SQL 3-5</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/yns-dev-exercises/assets/bootstrap.min.css">
    <script src="/yns-dev-exercises/assets/jquery.min.js"></script>
    <script src="/yns-dev-exercises/assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../components/navbar.php';
    ?>
    <div class="container" style="margin-top: 25px;">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 text-center">
                <h2>Registration</h2>
                <?php
                if (isset($message)) {
                    echo "<tr><td colspan='2'><h3 style='color: " . $messageColor . "'>" . $message . "</h3></td></tr>";
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <form method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group">
                        <label for="username" class="control-label col-sm-4"><b>Username: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="username" name="user[uid]" value="<?= isset($user['uid']) ? $user['uid'] : '' ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label col-sm-4"><b>Password: </b></label>
                        <div class="col-sm-6">
                            <input type="password" id="password" name="user[pass]" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cpassword" class="control-label col-sm-4"><b>Confirm Password: </b></label>
                        <div class="col-sm-6">
                            <input type="password" id="cpassword" name="user[cpass]" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fname" class="control-label col-sm-4"><b>First name: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="fname" name="user[fname]" value="<?= isset($user['fname']) ? $user['fname'] : '' ?>" placeholder="Juan" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="control-label col-sm-4"><b>Middle name: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="mname" name="user[mname]" value="<?= isset($user['mname']) ? $user['mname'] : '' ?>" placeholder="Zulueta" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lname" class="control-label col-sm-4"><b>Last name: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="lname" name="user[lname]" value="<?= isset($user['lname']) ? $user['lname'] : '' ?>" placeholder="Dela Cruz" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="age" class="control-label col-sm-4"><b>Age: </b></label>
                        <div class="col-sm-6">
                            <input type="number" id="age" name="user[age]" value="<?= isset($user['age']) ? $user['age'] : '' ?>" placeholder="21" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="gender" class="control-label col-sm-4"><b>Gender: </b></label>
                        <div class="col-sm-6">
                            <input type="radio" id="gender" name="user[gender]" value="male" <?= (isset($user['gender']) && $user['gender'] === 'male') ? 'checked' : '' ?> required> Male
                            <input type="radio" id="gender" name="user[gender]" value="female" <?= (isset($user['gender']) && $user['gender'] === 'female') ? 'checked' : '' ?> required> Female
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address" class="control-label col-sm-4"><b>Address: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="address" name="user[address]" value="<?= isset($user['address']) ? $user['address'] : '' ?>" placeholder="Block 123, XYZ Subdivision, ABC City" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="birthdate" class="control-label col-sm-4"><b>Birth Date: </b></label>
                        <div class="col-sm-6">
                            <input type="date" id="birthdate" name="user[birthdate]" value="<?= isset($user['birthdate']) ? $user['birthdate'] : '' ?>" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="contact" class="control-label col-sm-4"><b>Contact: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="contact" name="user[contact]" value="<?= isset($user['contact']) ? $user['contact'] : '' ?>" placeholder="0912-345-6789" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label col-sm-4"><b>Email: </b></label>
                        <div class="col-sm-6">
                            <input type="text" id="email" name="user[email]" value="<?= isset($user['email']) ? $user['email'] : '' ?>" placeholder="juandelacruz@xyz.com" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="picture" class="control-label col-sm-4"><b>picture: </b></label>
                        <div class="col-sm-6">
                            <input type="file" id="picture" name="picture" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <button type="submit" name="submit" class="btn btn-primary col-sm-12">Submit</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <a href="3-5.login.php">
                                <button type="button" name="back" class="btn btn-success col-sm-12">Back</button>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>

</html>