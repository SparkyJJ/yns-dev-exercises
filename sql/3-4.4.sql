SELECT
    emp.last_name AS Employee,
    boss.last_name AS Boss
FROM
    employees AS emp
INNER JOIN employees AS boss
ON
    emp.boss_id = boss.id;