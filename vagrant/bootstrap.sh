# #!/usr/bin/env bash
sudo -i
#Install EPEL and REMI
wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
rpm -Uvh remi-release-6.rpm epel-release-6-8.noarch.rpm
echo "============EPEL & REMI SETUP FINISH!============"

#Workaround for metadata issue with EPEL Repository
sed -i "s/mirrorlist=https/mirrorlist=http/" /etc/yum.repos.d/epel.repo
echo "============EPEL & REMI SETUP WORKAROUND!============"

#Clean up and Update of Yum
# yum clean all
yum update -y
echo "============YUM UPDATE============"

#Install Yum utilities
yum install -y yum-utils
echo "============YUM UTIL INSTALL FINISH!============"

#Install PHP 5.6
yum-config-manager --enable remi-php56
yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo
echo "============PHP INSTALL FINISH!============"

#Install Apache and Mod_ssl
yum install -y httpd mod_ssl
echo "ServerName localhost" >> /etc/httpd/conf/httpd.conf
/sbin/chkconfig httpd on
echo "============APACHE INSTALL FINISH!============"

#Install MySQL 5.7
wget http://dev.mysql.com/get/mysql57-community-release-el6-7.noarch.rpm
yum localinstall -y mysql57-community-release-el6-7.noarch.rpm
yum repolist enabled | grep “mysql.*-community.*”
yum install -y mysql-community-server
echo "============MYSQL INSTALL FINISH!============"

#Install phpmyadmin
yum install -y phpmyadmin
echo "============PHPMYADMIN INSTALL FINISH!============"

#Change MySQL temporary password to "password"
# service mysqld start
# mysqld_safe --skip-grant-table --execute="UPDATE user SET authentication_string=PASSWORD('password') WHERE user='root'; FLUSH PRIVILEDGES;"

# Allow remote connections for MySQL Workbench
MYSQLCONF=$(cat <<EOF

[mysqld]
bind-address        = 0.0.0.0
EOF
)
echo "${MYSQLCONF}" >> /etc/my.cnf

#Open up ports in firewall
iptables -I INPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp --dport 3306 -j ACCEPT

service iptables save

service mysqld restart
service httpd restart
php -v
httpd -v
echo "============SETUP COMPLETE============"
exit
# rm -rf /var/www
# ln -fs /vagrant /var/www
# echo "ServerName server.local" > /etc/httpd/conf/httpd.conf
# a2enmod rewrite
# mv /etc/php/7.2/apache2/php.ini /etc/php/7.2/apache2/php.ini.back
# cp /usr/lib/php/7.2/php.ini-development /etc/php/7.2/apache2/php.ini

# apachectl restart
# mysql.service start
# mysqladmin -u root password 'root'
# mysql.service restart
# echo "============LAMP INIT FINISH!============"
