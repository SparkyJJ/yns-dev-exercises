<?php
session_start();
// if (isset($_SESSION["last_activity"]) && (time() - $_SESSION["last_activity"] > 5)) {
//     session_destroy();
//     session_unset();
// }
// $_SESSION['last_activity'] = time();

$calendar = null;
if (isset($_POST['next'])) {
    if (isset($_SESSION['calendar'])) {
        extract($_SESSION['calendar']);
        if ($month < 11) {
            $month++;
        } else {
            $month = 0;
            $year++;
        }
        $_SESSION['calendar']['month'] = $month;
        $_SESSION['calendar']['year'] = $year;
        $calendar = displayCalendar($month, $year);
    }
} elseif (isset($_POST['back'])) {
    if (isset($_SESSION['calendar'])) {
        extract($_SESSION['calendar']);
        if ($month > 0) {
            $month--;
        } else {
            $month = 11;
            $year--;
        }
        $_SESSION['calendar']['month'] = $month;
        $_SESSION['calendar']['year'] = $year;
        $calendar = displayCalendar($month, $year);
    }
} elseif($calendar === null) {
    $calendar = displayCalendar();
}
function displayCalendar($m = null, $y = null)
{
    $dateNow = date('Y-m-d');
    if ($m === null || $y === null) {
        $m = date('m');
        $y = date('Y');
    }
    $dayStart = idate('w', mktime(0, 0, 0, $m, 1, $y));
    $endDate = idate('d', mktime(0, 0, 0, $m + 1, 0, $y));
    $display = '';

    $days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    $display .= '<tr>';
    foreach ($days as $value) {
        $display .= ("<td> $value </td>");
    }
    $display .= '</tr>';

    $startCount = false;
    $count = -$dayStart;
    for ($week = 0; $week < 6; $week++) {
        $display .= '<tr>';
        for ($day = 0; $day < 7; $day++) {
            $count++;

            if ($count > 0 && $count <= $endDate) {
                $value = strval($count);
            } else {
                $value = '';
            }

            $status = (strtotime("$y-$m-$count") == strtotime($dateNow)) ? ' class="success"' : '';

            $display .= "<td $status >$value</td>";
        }
        $display .= '</tr>';
    }
    $monthYear = date('F Y', mktime(0, 0, 0, $m, 1, $y));
    $_SESSION['calendar']['month'] = $m;
    $_SESSION['calendar']['year'] = $y;
    return array("header" => $monthYear, "body" => $display);
}

?>
<html>

<head>
    <title>Practice 5-2</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <script src="../../assets/jquery.min.js"></script>
    <script src="../../assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../../components/navbar.php';
    ?>

    <div class="container" style="margin-top:75px">
        <div class="col-sm-8 col-sm-offset-2">
            <form action="" method="POST">
                <table class="table table-striped" style="text-align: center">
                    <thead>
                        <th>
                            <button type="submit" name="back" class="btn btn-default">Back</button>
                        </th>
                        <th colspan="5" id="monthYear" style="text-align: center"><?= $calendar['header'] ?></th>
                        <th>
                            <button type="submit" name="next" class="btn btn-default">Next</button>
                        </th>
                    </thead>
                    <tbody id="calendar">
                        <?= $calendar['body'] ?>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
    <script>
        // $(window).on('load', function() {
        //     let month = new Date().getMonth();
        //     let year = new Date().getFullYear();
        //     displayCalendar(month, year);

        //     $("#back").click(function() {
        //         if (month > 0) {
        //             month--;
        //         } else {
        //             month = 11;
        //             year--;
        //         }
        //         displayCalendar(month, year);
        //     });

        //     $("#next").click(function() {
        //         if (month < 11) {
        //             month++;
        //         } else {
        //             month = 0;
        //             year++;
        //         }
        //         displayCalendar(month, year);
        //     });
        // });

        // function displayCalendar(m, y) {
        //     let currentMonth = new Date().getMonth();
        //     let currentYear = new Date().getFullYear();
        //     let currentDay = new Date().getDate();
        //     let dayStart = new Date(y, m, 1).getDay()
        //     let endDate = new Date(y, m, 0).getDate();
        //     let display = "";

        //     let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        //     let months = [
        //         "January", "February", "March",
        //         "April", "May", "June", "July",
        //         "August", "September", "October",
        //         "November", "December"
        //     ]

        //     display += "<tr>";
        //     for (let d of days) {
        //         display += ("<td>" + d + "</td>");
        //     }
        //     display += "</tr>";

        //     let start = false;
        //     let count = 0;
        //     for (let week = 0; week < 6; week++) {
        //         display += "<tr>";
        //         for (let day = 0; day < 7; day++) {
        //             if (dayStart === day && endDate > count) {
        //                 start = true;
        //             }

        //             let status = (
        //                 start &&
        //                 currentDay === count + 1 &&
        //                 currentMonth === m &&
        //                 currentYear === y
        //             ) ? " class='success'" : "";
        //             display += "<td" + status + ">";

        //             if (start) {
        //                 count++;
        //                 display += String(count);
        //             }

        //             if (endDate <= count) {
        //                 start = false;
        //             }

        //             display += "</td>";
        //         }
        //         display += "</tr>";
        //     }
        //     $("#calendar").html(display);
        //     $("#monthYear").html(months[m] + " " + y)
        // }
    </script>
</body>

</html>