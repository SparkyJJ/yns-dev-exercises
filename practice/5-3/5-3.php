<html>

<head>
    <title>Practice 5-3</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <script src="assets/jquery.min.js"></script>
    <script src="assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../../components/navbar.php';
    ?>

    <div class="container" style="margin:50px">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse1">
                        <h4 class="panel-title">
                            <span>HTML and PHP</span>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="../../htmlphp/1-1.php" class="list-group-item">1-1 Show Hello World.</a>
                                <a href="../../htmlphp/1-2.php" class="list-group-item">1-2 The four basic operations of
                                    arithmetic.</a>
                                <a href="../../htmlphp/1-3.php" class="list-group-item">1-3 Show the greatest common
                                    divisor.</a>
                                <a href="../../htmlphp/1-4.php" class="list-group-item">1-4 Solve FizzBuzz problem.</a>
                                <a href="../../htmlphp/1-5.php" class="list-group-item">1-5 Input date. Then show 3 days
                                    from inputted date and
                                    its day of the week.</a>
                                <a href="../../htmlphp/1-6.php" class="list-group-item">1-6 Input user information. Then
                                    show it in next
                                    page.</a>
                                <a href="../../htmlphp/1-7.php" class="list-group-item">1-7 Add validation in the user
                                    information form(required,
                                    numeric, character,
                                    mailaddress).</a>
                                <a href="../../htmlphp/1-8.php" class="list-group-item">1-8 Store inputted user
                                    information
                                    into a CSV file.</a>
                                <a href="../../htmlphp/1-9.php" class="list-group-item">1-9 Show the user information
                                    using
                                    table tags.</a>
                                <a href="../../htmlphp/1-10.php" class="list-group-item">1-10 Upload images.</a>
                                <a href="../../htmlphp/1-11.php" class="list-group-item">1-11 Show uploaded images in
                                    the
                                    table.</a>
                                <a href="../../htmlphp/1-12.php" class="list-group-item">1-12 Add pagination in the list
                                    page.</a>
                                <a href="../../htmlphp/1-13.php" class="list-group-item">1-13 Create login form and
                                    embed it
                                    into the system that
                                    you developed</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse2">
                        <h4 class="panel-title">
                            <span>Javascript</span>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="../../javascript/2-1.html" class="list-group-item">2-1 Show alert.</a>
                                <a href="../../javascript/2-2.html" class="list-group-item">2-2 Confirm dialog and
                                    redirection</a>
                                <a href="../../javascript/2-3.html" class="list-group-item">2-3 The four basic operations
                                    of
                                    arithmetic</a>
                                <a href="../../javascript/2-4.html" class="list-group-item">2-4 Show prime numbers.</a>
                                <a href="../../javascript/2-5.html" class="list-group-item">2-5 Input characters in text
                                    box
                                    and show it in label.</a>
                                <a href="../../javascript/2-6.html" class="list-group-item">2-6 Press button and add a
                                    label
                                    below button.</a>
                                <a href="../../javascript/2-7.html" class="list-group-item">2-7 Show alert when you click
                                    an
                                    image.</a>
                                <a href="../../javascript/2-8.html" class="list-group-item">2-8 Show alert when you click
                                    link.</a>
                                <a href="../../javascript/2-9.html" class="list-group-item">2-9 Change text and
                                    background
                                    color when you press buttons.</a>
                                <a href="../../javascript/2-10.html" class="list-group-item">2-10 Scroll screen when you
                                    press buttons.</a>
                                <a href="../../javascript/2-11.html" class="list-group-item">2-11 Change background color
                                    using animation.</a>
                                <a href="../../javascript/2-12.html" class="list-group-item">2-12 Show another image when
                                    you
                                    mouse over an image. Then show the original image when you mouse out.</a>
                                <a href="../../javascript/2-13.html" class="list-group-item">2-13 Change size of images
                                    when
                                    you press buttons.</a>
                                <a href="../../javascript/2-14.html" class="list-group-item">2-14 Show images according
                                    to
                                    the options in combo box.</a>
                                <a href="../../javascript/2-15.html" class="list-group-item">2-15 Show current date and
                                    time
                                    in real time.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse4">
                        <h4 class="panel-title">
                            <span>SQL</span>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="../../sql/3-5.login.php" class="list-group-item">3-5 Use database in the
                                    applications
                                    that you developed.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse5">
                        <h4 class="panel-title">
                            <span>Vagrant</span>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="http://192.168.1.100/1-13/1-13.login.php" class="list-group-item">Application
                                    1-13 on Vagrant</a>
                                <a href="http://192.168.1.100/3-5/3-5.login.php" class="list-group-item">Application 3-5
                                    on Vagrant</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" data-target="#collapse6">
                        <h4 class="panel-title">
                            <span>Practice</span>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="../5-1/5-1.php" class="list-group-item">5-1 Quiz with three multiple
                                    choices</a>
                                <a href="../5-2/5-2.php" class="list-group-item">5-2 Calendar</a>
                                <a href="#" class="list-group-item">5-3 Navigation</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(window).on('load', function() {
            let month = new Date().getMonth();
            let year = new Date().getFullYear();
            displayCalendar(month, year);

            $("#back").click(function() {
                if (month > 0) {
                    month--;
                } else {
                    month = 11;
                    year--;
                }
                displayCalendar(month, year);
            });

            $("#next").click(function() {
                if (month < 11) {
                    month++;
                } else {
                    month = 0;
                    year++;
                }
                displayCalendar(month, year);
            });
        });

        function displayCalendar(m, y) {
            let currentMonth = new Date().getMonth();
            let currentYear = new Date().getFullYear();
            let currentDay = new Date().getDate();
            let dayStart = new Date(y, m, 1).getDay()
            let endDate = new Date(y, m, 0).getDate();
            let display = "";

            let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            let months = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
            ]

            display += "<tr>";
            for (let d of days) {
                display += ("<td>" + d + "</td>");
            }
            display += "</tr>";

            let start = false;
            let count = 0;
            for (let week = 0; week < 6; week++) {
                display += "<tr>";
                for (let day = 0; day < 7; day++) {
                    if (dayStart === day && endDate > count) {
                        start = true;
                    }

                    let status = (
                        start &&
                        currentDay === count + 1 &&
                        currentMonth === m &&
                        currentYear === y
                    ) ? " class='success'" : "";
                    display += "<td" + status + ">";

                    if (start) {
                        count++;
                        display += String(count);
                    }

                    if (endDate <= count) {
                        start = false;
                    }

                    display += "</td>";
                }
                display += "</tr>";
            }
            $("#calendar").html(display);
            $("#monthYear").html(months[m] + " " + y)
        }
    </script>
</body>

</html>