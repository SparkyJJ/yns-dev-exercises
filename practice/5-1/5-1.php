<?php
require_once 'connect.php';
define('IMAGE_DIRECTORY', 'quiz_images/');
session_start();

$query = 'SELECT
q.id,
q.quiz_item,
q.image,
GROUP_CONCAT(CONCAT(a.id, "|", a.answer_item) SEPARATOR "$$") as answers
FROM
questions AS q
LEFT JOIN answers AS a
ON
q.id = a.quiz_id
GROUP BY q.id
ORDER BY
RAND()
LIMIT
10';

$stmt = $connect->prepare($query);
$stmt->execute();
$queryResult = $stmt->get_result();
$stmt->fetch();
$stmt->close();
if ($queryResult->num_rows > 0) {
    while ($row = $queryResult->fetch_assoc()) {
        $temp1 = explode("$$", $row['answers']);
        shuffle($temp1);
        $row['answers'] = [];
        foreach ($temp1 as $value) {
            $temp2 = explode("|", $value);
            $temp3['id'] = $temp2[0];
            $temp3['answer_item'] = $temp2[1];
            $row['answers'][] = $temp3;
        }
        $data[] = $row;
    }
}
$_SESSION['quiz'] = $data;
?>
<html>

<head>
    <title>[5-1] Quiz</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <script src="assets/jquery.min.js"></script>
    <script src="assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../../components/navbar.php';
    ?>

    <div class="container" style="margin-top:5px">
        <h1 class="text-center">Circuits Quizmaster!</h1>
        <div class="col-sm-8 col-sm-offset-2">
            <form action="5-1.score.php" method="POST">
                <table class="table table-striped">
                    <thead>
                    </thead>
                    <tbody>
                        <?php
                        $num = 1;
                        foreach ($data as $value) {
                            extract($value);
                            echo "<tr>";
                            echo "<td>";
                            echo "<p>$num. $quiz_item</p>";
                            if (isset($image)) {
                                $directory = IMAGE_DIRECTORY . $image;
                                echo "<div class='container-fluid' style='text-align: center;'><img src='$directory'></div>";
                            }
                            foreach ($answers as $ans) {
                                echo "<div class='radio'>";
                                echo "<label><input type='radio' name='question_$id' value='" . $ans['id'] . "'>" .
                                    $ans['answer_item'] . "</label>";
                                echo "</div>";
                            }
                            echo "</td>";
                            echo "</tr>";
                            $num++;
                        }
                        ?>
                    </tbody>
                </table>
                <center>
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
                </center>
            </form>
        </div>
    </div>
</body>

</html>