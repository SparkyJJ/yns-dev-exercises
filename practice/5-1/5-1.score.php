<?php
require_once 'connect.php';
define('IMAGE_DIRECTORY', 'quiz_images/');
session_start();

if (isset($_POST['submit'])) {
    $display = $_SESSION['quiz'];
    $query = 'SELECT
        id,
        quiz_id
    FROM
        answers
    WHERE 
        correct = true 
    AND
        quiz_id
    IN (';
    foreach ($display as $value) {
        $array_id[] = $value['id'];
    }
    $query .= implode(',', $array_id) . ')';

    $stmt = $connect->prepare($query);
    $stmt->execute();
    $queryResult = $stmt->get_result();
    $stmt->fetch();
    $stmt->close();
    if ($queryResult->num_rows > 0) {
        while ($row = $queryResult->fetch_assoc()) {
            $key = "question_" . $row['quiz_id'];
            $data[$key] = $row['id'];
        }
    }
} else {
    header('Location: 5-1.php');
}
?>

<html>

<head>
    <title>[5-1] Quiz</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="assets/bootstrap.min.css">
    <script src="assets/jquery.min.js"></script>
    <script src="assets/bootstrap.min.js"></script>
</head>

<body>
    <?php
    include_once '../../components/navbar.php';
    ?>

    <div class="container" style="margin-top:5px">
        <h1 class="text-center">Circuits Quizmaster!</h1>
        <div class="col-sm-8 col-sm-offset-2">
            <table class="table table-striped">
                <thead>
                </thead>
                <tbody>
                    <?php
                    $score = 0;
                    $num = 1;
                    foreach ($display as $value) {
                        $status = false;
                        extract($value);
                        $key = "question_" . $id;
                        if (isset($_POST[$key])) {
                            if ($data[$key] == $_POST[$key]) {
                                $score++;
                                $status = true;
                            }
                        }
                        echo "<tr class='";
                        echo ($status ? "success" : "danger");
                        echo "'>";
                        echo "<td>";
                        echo "<p>$num. $quiz_item</p>";
                        if (isset($image)) {
                            $directory = IMAGE_DIRECTORY . $image;
                            echo "<div class='container-fluid' style='text-align: center; margin: 5px;'><img src='$directory'></div>";
                        }
                        foreach ($answers as $ans) {
                            if ($data[$key] === (int) $ans['id']) {
                                echo "<p>Answer is: " . $ans['answer_item'] . "</p>";
                            }
                        }
                        echo "</td>";
                        echo "</tr>";
                        $num++;
                    }
                    ?>
                </tbody>
            </table>
            <center>
                <a href="5-1.php">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">RETRY</button>
                </a>
            </center>
        </div>
    </div>

    <div id="scoreModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Circuits Quizmaster</h4>
                </div>
                <div class="modal-body">
                    <p>Your score is: <?= $score ?></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</body>
<script type="text/javascript">
    $(window).on('load', function() {
        <?php
        if (isset($_POST['submit'])) {
            ?>
            $('#scoreModal').modal('show');
        <?php
        }
        ?>
    });
</script>

</html>