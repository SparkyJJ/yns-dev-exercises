<?php
$serverName = "localhost";
$userName = "root";
$password = "";
$database = "5-1";

// Create connection
$connect = new mysqli($serverName, $userName, $password, $database);

// Check connection
if ($connect->connect_error) {
    die("Connection failed: " . $connect->connect_error);
}
?>