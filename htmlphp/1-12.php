<?php
define('IMAGE_ITEM', 7);
define('MAX_NUMBER_OF_ITEMS', 7);
define('IMAGE_DIRECTORY', 'profile_picture/');
define('DEFAULT_IMAGE', 'default.png');
define('CSV_FILE', 'file.csv');
define('NUM_PER_PAGE', 10);

$handle = fopen(CSV_FILE, "r");
$page = isset($_GET['page']) ? $_GET['page'] : 1;
$count = 0;
$start = (($page - 1) * NUM_PER_PAGE) + 1;
$end = $start + NUM_PER_PAGE - 1;
$display = array();
if ($handle) {
    while (!feof($handle)) {
        $data = fgetcsv($handle);
        if ($data) $count++;
        if (($count >= $start && $count <= $end) && array(null) !== $data && $data) {
            $display[] = $data;
        }
    }
}
$totalPage = ceil($count / NUM_PER_PAGE);
fclose($handle);
?>
<html>

<head>
    <title>HTML & PHP 1-12</title>
</head>

<body>
    <div>
        <div>
            <table cellpadding="10" align="center" style="margin-top: 20px; margin-bottom:20px; border: 2px solid #ddd">
                <thead>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Contact Number</th>
                    <th>Email</th>
                </thead>
                <tbody>
                    <?php
                    $id = $start;
                    foreach ($display as $data) {
                        echo "<tr>";
                        echo "<td>" . $id . "</td>";
                        echo "<td>";
                        echo "<img src='";
                        if (isset($data[IMAGE_ITEM])) {
                            echo IMAGE_DIRECTORY . $data[IMAGE_ITEM];
                        } else {
                            echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                        }
                        echo "' style='max-width: 50px'>";
                        echo "</td>";
                        $i = 0;
                        foreach ($data as $value) {
                            if ($i < MAX_NUMBER_OF_ITEMS) echo "<td>" . $value . "</td>";
                            $i++;
                        }
                        echo "</tr>";
                        $id++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <hr>
        <div>
            <center>
                <?php
                if ($page > 1) {
                    echo "<a href='?page=" . ($page - 1) . "'>PREVIOUS</a>&ensp;";
                } else {
                    echo "PREVIOUS&ensp;";
                }

                for ($i = 1; $i <= $totalPage; $i++) {
                    if ($i != $page) {
                        echo "<a href='?page=" . $i . "'>" . $i . "</a>&ensp;";
                    } else {
                        echo $i . "&ensp;";
                    }
                }

                if ($page < $totalPage) {
                    echo "<a href='?page=" . ($page + 1) . "'>NEXT</a>";
                } else {
                    echo "NEXT";
                }
                ?>
            </center>
        </div>
    </div>
</body>

</html>