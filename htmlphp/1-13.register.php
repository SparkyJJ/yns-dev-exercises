<?php
define('CSV_FILE', '1-13.users.csv');
define('IMAGE_DIRECTORY', 'profile_picture/');

$message = null;
$messageColor = null;
if (isset($_POST['submit'])) {
    extract($_POST);
    $isValid = true;
    foreach ($user as $key => $value) {
        if (empty($value)) { //Validate empty fields.
            echo $value;
            $isValid = false;
            $message = 'Please complete the information form.';
            break;
        } elseif ($key == 'uid' || $key == 'pass') { //Prevents whitespaces in username/password
            if (preg_match("/\s/", $value)) {
                $isValid = false;
                $message = 'Whitespace is not allowed for username/password.';
                break;
            }

            if ($key == 'uid') {
                $handle = fopen(CSV_FILE, 'r');
                if ($handle) {
                    while (!feof($handle)) {
                        $data = fgetcsv($handle);
                        if ($data[0] == $value) {
                            $isValid = false;
                            $message = 'Username already taken.';
                            break;
                        }
                    }
                }
                fclose($handle);
            }

            if ($key == 'pass' && strlen($value) < 8) { //Validates string length of password
                $isValid = false;
                $message = 'Password should be at least 8 characters';
                break;
            }
        } elseif ($key == 'fname' || $key == 'lname') { //Characters only in names
            if (!preg_match("/^[a-zA-Z ]*$/", $value)) {
                $isValid = false;
                $message = 'Invalid character in name.';
                break;
            }
        } elseif ($key == 'age') { //Characters only in names
            if (!preg_match("/^[0-9]+$/", $value)) {
                $isValid = false;
                $message = 'Numeric characters only for age.';
                break;
            }
        } elseif ($key == 'contact') { //Validates contact number format
            if (!preg_match("/^\d{4}-\d{3}-\d{4}$/", $value)) {
                $isValid = false;
                $message = 'Invalid contact number format.';
                break;
            }
        } elseif ($key == 'email') { //Validates email through regex
            if (!preg_match("/^[a-zA-Z0-9]+\@[a-zA-Z0-9]+\.[a-z]{2,3}$/", $value)) {
                $isValid = false;
                $message = 'Invalid email format.';
                break;
            }
        }
    }

    if ($user['pass'] !== $user['cpass']) { //Checks if password and confirm password is the same
        $isValid = false;
        $message = 'Password does not match confirm password.';
    } elseif (!empty($_FILES['picture']['name'])) { //Checks if an image is uploaded
        $imageFileType = strtolower(pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION));
        $filename = date('Y-m-d') . '-' . round(microtime(true)) . '.' . $imageFileType;
        $targetFile = IMAGE_DIRECTORY . $filename;
        $check = getimagesize($_FILES['picture']['tmp_name']);
        if ( //Image related validation
            $check == false || file_exists($targetFile)
            || $_FILES['picture']['size'] > 500000
            || ($imageFileType != 'jpg' && $imageFileType != 'png'
                && $imageFileType != 'jpeg'
                && $imageFileType != 'gif')
        ) {
            $message = 'Invalid image upload.';
            $isValid = false;
        } elseif (!move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile)) { //Uploads file to directory
            $message = 'Error uploading your image.';
            $isValid = false;
        } else {
            $user['pictureID'] = $filename;
        }
    } else {
        $user['pictureID'] = null;
    }

    if (!$isValid) {
        $messageColor = 'red';
    } else {
        $user['pass'] = password_hash($user['pass'], PASSWORD_DEFAULT);
        unset($user['cpass']);
        $csv = fopen(CSV_FILE, 'a') or die('Unable to open file!');
        fputcsv($csv, $user);
        fclose($csv);
        $message = 'Submit successful!';
        $messageColor = 'green';
    }
}
?>

<html>

<head>
    <title>HTML & PHP 1-13 Registration</title>
</head>

<body>
    <div style="margin-top: 20px;">
        <table cellpadding="8" align="center" style="border-collapse: collapse">
            <thead>
                <th colspan="2">
                    <h2>Registration</h2>
                </th>
            </thead>
            <tbody>
                <?php
                if (isset($message)) {
                    echo "<tr><td colspan='2'><h3 style='color: " . $messageColor . "'>" . $message . "</h3></td></tr>";
                }
                ?>
                <form method="POST" enctype="multipart/form-data">
                    <tr>
                        <td><label> Username: </label></td>
                        <td><input type="text" name="user[uid]"></td>
                    </tr>
                    <tr>
                        <td><label> Password: </label></td>
                        <td><input type="password" name="user[pass]"></td>
                    </tr>
                    <tr>
                        <td><label> Confirm Password: </label></td>
                        <td><input type="password" name="user[cpass]"></td>
                    </tr>
                    <tr style="border-top: 2px solid #bbb">
                        <td><label> First Name: </label></td>
                        <td><input type="text" name="user[fname]" placeholder="Juan"></td>
                    </tr>
                    <tr>
                        <td><label> Last Name: </label></td>
                        <td><input type="text" name="user[lname]" placeholder="Dela Cruz"></td>
                    </tr>
                    <tr>
                        <td><label> Age: </label></td>
                        <td><input type="number" name="user[age]" placeholder="21"></td>
                    </tr>
                    <tr>
                        <td><label> Gender: </label></td>
                        <td><input type="radio" name="user[gender]" value="male" checked> Male
                            <input type="radio" name="user[gender]" value="female"> Female</td>
                    </tr>
                    <tr>
                        <td><label> Address: </label></td>
                        <td><input type="text" name="user[address]" placeholder="Block 123, XYZ Subdivision, ABC City"></td>
                    </tr>
                    <tr>
                        <td><label> Contact Number: </label></td>
                        <td><input type="text" name="user[contact]" placeholder="0912-345-6789"></td>
                    </tr>
                    <tr>
                        <td><label> Email: </label></td>
                        <td><input type="text" name="user[email]" placeholder="juandelacruz@xyz.com"></td>
                    </tr>
                    <tr>
                        <td><label> Upload Profile Picture: </label></td>
                        <td><input type="file" name="picture" id="picture"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button type="submit" name="submit" style="width: 100%">Submit</button></td>
                    </tr>
                </form>
                <form action="1-13.login.php" method="post">
                    <tr>
                        <td colspan="2"><button type="submit" name="back" style="width: 100%">Back</button></td>
                    </tr>
                </form>
            </tbody>
        </table>
    </div>

</body>

</html>