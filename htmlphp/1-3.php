<?php
function gcd($num1, $num2)
{
	if ($num1 === 0)
		return $num2;
	return gcd($num2 % $num1, $num1);
}

if (isset($_POST['calc'])) {
	extract($_POST);
	$result = "GCD of $num1 and $num2 is " . gcd($num1, $num2);
}
?>

<html>

<head>
	<title>HTML & PHP 1-3</title>
</head>

<body>
	<form method="POST">
		<div style="padding: 50px">
			<table cellpadding="10" align="center">
				<tbody>
					<tr>
						<td>
							<label> 1st Number: </label>
						</td>
						<td>
							<input type="number" name="num1">
						</td>
					</tr>
					<tr>
						<td>
							<label> 2nd Number: </label>
						</td>
						<td>
							<input type="number" name="num2">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<button type="submit" name="calc" value="add" style="width: 100%">GCD</button>
						</td>
					</tr>
					<?php
						echo "<tr align='center'><td colspan='2'><h3>$result</h3></td></tr>";
					?>
				</tbody>
			</table>
		</div>
	</form>

</body>

</html>