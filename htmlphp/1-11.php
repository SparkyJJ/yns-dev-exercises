<?php
define('IMAGE_ITEM', 7);
define('MAX_NUMBER_OF_ITEMS', 7);
define('IMAGE_DIRECTORY', 'profile_picture/');
define('DEFAULT_IMAGE', 'default.png');
define('CSV_FILE', 'file.csv');
?>
<html>

<head>
    <title>HTML & PHP 1-11</title>
</head>

<body>
    <table cellpadding="10" align="center" style="margin-top: 20px; margin-bottom:20px; border: 2px solid #ddd">
        <thead>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Contact Number</th>
            <th>Email</th>
        </thead>
        <tbody>
            <?php
            $id = 1;
            if (($handle = fopen(CSV_FILE, "r")) !== FALSE) {
                while (($data = fgetcsv($handle)) !== FALSE) {
                    if (array(null) !== $data) {
                        echo "<tr>";
                        echo "<td> $id </td>";
                        echo "<td>";
                        echo "<img src='";
                        if (isset($data[IMAGE_ITEM])) {
                            echo IMAGE_DIRECTORY . $data[IMAGE_ITEM];
                        } else {
                            echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                        }
                        echo "' style='max-width: 50px'>";
                        echo "</td>";
                        $i = 0;
                        foreach ($data as $value) {
                            if ($i < MAX_NUMBER_OF_ITEMS) echo "<td>" . $value . "</td>";
                            $i++;
                        }
                        echo "</tr>";
                        $id++;
                    }
                }
                fclose($handle);
            }

            ?>
        </tbody>
    </table>
</body>

</html>