<?php
define('IMAGE_ITEM', 9);
define('MAX_NUMBER_OF_ITEMS', 9);
define('IMAGE_DIRECTORY', 'profile_picture/');
define('DEFAULT_IMAGE', 'default.png');
define('CSV_FILE', '1-13.users.csv');
define('NUM_PER_PAGE', 10);

session_start();
if (!$_SESSION['access']) {
    header("Location: 1-13.login.php?access=invalid");
} else {
    $username = $_SESSION['username'];
    $password = $_SESSION['password'];

    $handle = fopen(CSV_FILE, "r");
    $page = isset($_GET['page']) ? $_GET['page'] : 1;
    $count = 0;
    $start = (($page - 1) * NUM_PER_PAGE) + 1;
    $end = $start + NUM_PER_PAGE - 1;
    $userInfo;
    $display = array();
    if ($handle) {
        while (!feof($handle)) {
            $data = fgetcsv($handle);
            if ($data) {
                $count++;
            }
            if ($data[0] == $username) {
                $userInfo = $data;
            }
            if (($count >= $start && $count <= $end) && array(null) !== $data && $data) {
                $display[] = $data;
            }
        }
    }
    $totalPage = ceil($count / NUM_PER_PAGE);
    fclose($handle);
}

if (isset($_POST['logout'])) {
    unset($_SESSION['username']);
    unset($_SESSION['password']);
    unset($_SESSION['access']);
    session_destroy();
    header("Location: 1-13.login.php");
    exit();
}
?>
<html>

<head>
    <title>HTML & PHP 1-13</title>
</head>

<body>
    <div>
        <div>
            <form method="post">
                <table cellpadding="10" align="center">
                    <thead>
                        <tr>
                            <th colspan="5">
                                <h2>Welcome <?= $userInfo[2] . " " . $userInfo[3] . "!" ?></h2>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="2">
                                <?php
                                echo "<img src='";
                                if (isset($userInfo[IMAGE_ITEM]) && $userInfo[IMAGE_ITEM] !== "") {
                                    echo IMAGE_DIRECTORY . $userInfo[IMAGE_ITEM];
                                } else {
                                    echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                                }
                                echo "' style='max-width: 50px'>";
                                ?>
                            </td>
                            <td><?= "Age: " . $userInfo[4] ?></td>
                            <td><?= "Gender: " . ucfirst($userInfo[5]) ?></td>
                            <td><?= "Address: " . $userInfo[6] ?></td>
                            <td rowspan="2">
                                <?= "<button type='submit' name='logout'>Logout</button> " ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= "Contact: " . $userInfo[7] ?></td>
                            <td><?= "Email: " . $userInfo[8] ?></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="10" align="center" style="margin-top: 20px; margin-bottom:20px; border: 2px solid #ddd">
                    <thead>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>Username</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Address</th>
                        <th>Contact Number</th>
                        <th>Email</th>
                    </thead>
                    <tbody>
                        <?php
                        $id = $start;
                        foreach ($display as $data) {
                            echo "<tr>";
                            echo "<td>" . $id . "</td>";
                            echo "<td>";
                            echo "<img src='";
                            if (isset($data[IMAGE_ITEM]) && $data[IMAGE_ITEM] !== "") {
                                echo IMAGE_DIRECTORY . $data[IMAGE_ITEM];
                            } else {
                                echo IMAGE_DIRECTORY . DEFAULT_IMAGE;
                            }
                            echo "' style='max-width: 50px'>";
                            echo "</td>";
                            $i = 0;
                            for ($i = 0; $i < MAX_NUMBER_OF_ITEMS; $i++) {
                                if ($i != 1) {
                                    echo "<td>" . $data[$i] . "</td>";
                                }
                            }
                            echo "</tr>";
                            $id++;
                        }
                        ?>
                    </tbody>
                </table>
            </form>
        </div>
        <hr>
        <div>
            <center>
                <?php
                if ($page > 1) echo "<a href='?page=" . ($page - 1) . "'>PREVIOUS</a>&ensp;";
                else echo "PREVIOUS&ensp;";
                for ($i = 1; $i <= $totalPage; $i++) {
                    if ($i != $page) echo "<a href='?page=" . $i . "'>" . $i . "</a>&ensp;";
                    else echo $i . "&ensp;";
                }
                if ($page < $totalPage) echo "<a href='?page=" . ($page + 1) . "'>NEXT</a>";
                else echo "NEXT";
                ?>
            </center>
        </div>
    </div>
</body>

</html>