<html>

<head>
	<title>HTML & PHP 1-6</title>
</head>

<body>
	<form action="1-6.info.php" method="POST">
		<table cellpadding="10" align="center">
			<thead>
				<th colspan="2">User Information Form (No validation)</th>
			</thead>
			<tbody>
				<tr>
					<td>
						<label> First Name: </label>
					</td>
					<td>
						<input type="text" name="fname">
					</td>
				</tr>
				<tr>
					<td>
						<label> Last Name: </label>
					</td>
					<td>
						<input type="text" name="lname">
					</td>
				</tr>
				<tr>
					<td>
						<label> Age: </label>
					</td>
					<td>
						<input type="number" name="age">
					</td>
				</tr>
				<tr>
					<td>
						<label> Gender: </label>
					</td>
					<td>
						<input type="radio" name="gender" value="male"> Male
						<input type="radio" name="gender" value="female"> Female
					</td>
				</tr>
				<tr>
					<td>
						<label> Address: </label>
					</td>
					<td>
						<input type="text" name="address">
					</td>
				</tr>
				<tr>
					<td>
						<label> Contact Number: </label>
					</td>
					<td>
						<input type="text" name="contact">
					</td>
				</tr>
				<tr>
					<td>
						<label> Email: </label>
					</td>
					<td>
						<input type="text" name="email">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<button type="submit" name="submit" style="width: 100%">Submit</button>
					</td>
				</tr>
			</tbody>

		</table>
		<br>
	</form>
</body>

</html>