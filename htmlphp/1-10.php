<?php
define('CSV_FILE', 'file.csv');
define('IMAGE_DIRECTORY', 'profile_picture/');

$message = null;
$messageColor = null;
if (isset($_POST['submit'])) {
    extract($_POST);
    $isValid = true;
    foreach ($user as $key => $value) {
        if (empty($value)) {
            echo $value;
            $isValid = false;
            $message = 'Please complete the information form.';
            $messageColor = 'red';
            break;
        } elseif ($key == 'fname' || $key == 'lname') {
            if (!preg_match("/^[a-zA-Z ]*$/", $value)) {
                $isValid = false;
                $message = 'Invalid character in name.';
                $messageColor = 'red';
                break;
            }
        } elseif ($key == 'contact') {
            if (!preg_match("/^\d{4}-\d{3}-\d{4}$/", $value)) {
                $isValid = false;
                $message = 'Invalid contact number format.';
                $messageColor = 'red';
                break;
            }
        } elseif ($key == 'email') {
            if (!preg_match("/^[a-zA-Z0-9]+\@[a-zA-Z0-9]+\.[a-z]{2,3}$/", $value)) {
                $isValid = false;
                $message = 'Invalid email format.';
                $messageColor = 'red';
                break;
            }
        }
    }
    if (empty($_FILES['picture']['name'])) {
        $isValid = false;
        $message = 'Please upload an image.';
        $messageColor = 'red';
    } else {
        $imageFileType = strtolower(pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION));
        $filename = date('Y-m-d') . '-' . round(microtime(true)) . '.' . $imageFileType;
        $targetFile = IMAGE_DIRECTORY . $filename;
        $check = getimagesize($_FILES['picture']['tmp_name']);
        if (
            $check == false || file_exists($targetFile)
            || $_FILES['picture']['size'] > 500000
            || ($imageFileType != 'jpg'
                && $imageFileType != 'png'
                && $imageFileType != 'jpeg'
                && $imageFileType != 'gif')
        ) {
            $isValid = false;
            $message = 'Invalid image upload.';
            $messageColor = 'red';
        }
    }

    if ($isValid) {
        if (move_uploaded_file($_FILES['picture']['tmp_name'], $targetFile)) {
            $user['pictureID'] = $filename;
            $csv = fopen(CSV_FILE, 'a') or die('Unable to open file!');
            fputcsv($csv, $user);
            fclose($csv);
            $message = 'Submit successful!';
            $messageColor = 'green';
        } else {
            $message = 'Sorry, there was an error uploading your image.';
            $messageColor = 'red';
        }
    }
}
?>
<html>

<head>
    <title>HTML & PHP 1-10</title>
</head>

<body>
    <form method="POST" enctype="multipart/form-data">
        <table cellpadding="10" align="center">
            <thead>
                <th colspan="2">User Information Form (Store to CSV with Image)</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <label> First Name: </label>
                    </td>
                    <td>
                        <input type="text" name="user[fname]" placeholder="Juan">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Last Name: </label>
                    </td>
                    <td>
                        <input type="text" name="user[lname]" placeholder="Dela Cruz">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Age: </label>
                    </td>
                    <td>
                        <input type="number" name="user[age]" placeholder="21">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Gender: </label>
                    </td>
                    <td>
                        <input type="radio" name="user[gender]" value="male" checked> Male
                        <input type="radio" name="user[gender]" value="female"> Female
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Address: </label>
                    </td>
                    <td>
                        <input type="text" name="user[address]" placeholder="Block 123, XYZ Subdivision, ABC City">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Contact Number: </label>
                    </td>
                    <td>
                        <input type="text" name="user[contact]" placeholder="0912-345-6789">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Email: </label>
                    </td>
                    <td>
                        <input type="text" name="user[email]" placeholder="juandelacruz@xyz.com">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label> Upload Profile Picture: </label>
                    </td>
                    <td>
                        <input type="file" name="picture" id="picture"></p>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button type="submit" name="submit" style="width: 100%">Submit</button>
                    </td>
                </tr>
                <?php
                if (isset($message)) {
                    echo "<tr><td colspan='2' align='center'>";
                    echo "<h3 style='color: $messageColor'> $message </h3>";
                    echo "</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </form>

</body>

</html>