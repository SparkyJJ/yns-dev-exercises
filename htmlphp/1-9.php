<?php
define('CSV_FILE', 'file.csv');
define('MAX_NUMBER_OF_ITEMS', 7);
?>
<html>

<head>
    <title>HTML & PHP 1-9</title>
</head>

<body>
    <table cellpadding="10" align="center" style="margin-top: 20px; margin-bottom:20px; border: 2px solid #ddd">
        <thead>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Contact Number</th>
            <th>Email</th>
        </thead>
        <tbody>
            <?php
            if (($handle = fopen(CSV_FILE, 'r')) !== FALSE) {
                while (($data = fgetcsv($handle)) !== FALSE) {
                    if (array(null) !== $data) {
                        echo '<tr>';
                        for($i = 0; $i < MAX_NUMBER_OF_ITEMS; $i++) {
                            echo '<td>' . $data[$i] . '</td>';
                        }
                        echo '</tr>';
                    }
                }
                fclose($handle);
            }

            ?>
        </tbody>
    </table>
</body>

</html>