<?php
error_reporting(0);
if (isset($_POST['calc'])) {
	extract($_POST);
	$result = 0;
	switch ($calc) {
		case 'add':
			$result = $num1 + $num2;
			$operation = 'Addition';
			break;
		case 'sub':
			$result = $num1 - $num2;
			$operation = 'Subtraction';
			break;
		case 'div':
			$result = $num1 / $num2;
			$operation = 'Division';
			break;
		case 'mul':
			$result = $num1 * $num2;
			$operation = 'Multiplication';
			break;
	}
}
?>
	<html>

	<head>
		<title>HTML & PHP 1-2</title>
	</head>

	<body>
		<form method="POST">
			<div style="padding: 50px">
				<table cellpadding="5" align="center" style="border-collapse: collapse">
					<tbody>
						<tr>
							<td colspan="2">
								<label> 1st Number: </label>
							</td>
							<td colspan="2">
								<input type="number" name="num1">
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<label> 2nd Number: </label>
							</td>
							<td colspan="2">
								<input type="number" name="num2">
							</td>
						</tr>
						<tr>
							<td>
								<button type="submit" name="calc" value="add" style="width: 100%">Add</button>
							</td>
							<td>
								<button type="submit" name="calc" value="sub" style="width: 100%">Subtract</button>
							</td>
							<td>
								<button type="submit" name="calc" value="div" style="width: 100%">Divide</button>
							</td>
							<td>
								<button type="submit" name="calc" value="mul" style="width: 100%">Multiply</button>
							</td>
						</tr>
						<?php
						if (isset($operation)) {
							echo "<tr style='border-top: 2px solid #ddd'><td colspan='2'>Operation: </td>";
							echo "<td colspan='2'>$operation</td></tr>";
							echo "<tr><td colspan='2'>1st number: </td>";
							echo "<td colspan='2'>$num1</td></tr>";
							echo "<tr><td colspan='2'>2nd number: </td>";
							echo "<td colspan='2'>$num2</td></tr>";
							echo "<tr><td colspan='2'>Result: </td>";
							echo "<td colspan='2'>";
							echo (is_nan($result) || is_infinite($result)) ? 'Invalid operation' : $result;
							echo "</td></tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</form>

	</body>

	</html>