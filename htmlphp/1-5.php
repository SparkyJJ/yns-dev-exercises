<html>

<head>
	<title>HTML & PHP 1-5</title>
</head>

<body>
	<div style="padding: 50px">
		<center>
			<form method="POST">
				<label> Enter date: </label>
				<input type="date" name="date">
				<br>
				<br>
				<button type="submit" name="calc">Calculate!</button>
			</form>

			<?php
			if (isset($_POST['calc'])) {
				extract($_POST);
				if ($date) {
					echo "<h3 style='color: green'>Given date: $date <br></h3>";
					for ($i = 1; $i <= 3; $i++) {
						$incrementDate = strtotime($date . '+ ' . $i . ' days');
						echo date('l Y-m-d', $incrementDate) . '<br>';
					}
				}
			}
			?>
		</center>
	</div>
</body>

</html>