<html>

<head>
	<title>HTML & PHP 1-4</title>
</head>

<body>
	<div style="padding: 50px">
		<center>
			<form method="POST">
				<label> Enter a number: </label>
				<input type="number" name="num">
				<br>
				<br>
				<button type="submit" name="calc">FizzBuzz!</button>
			</form>
			<?php
			if (isset($_POST['calc'])) {
				extract($_POST);
				for ($i = 1; $i <= $num; $i++) {
					if ($i % 3 == 0 || $i % 5 == 0) {

						if ($i % 3 == 0) {
							echo "Fizz";
						};

						if ($i % 5 == 0) {
							echo "Buzz";
						}
					} else {
						echo $i;
					}
					echo "<br>";
				}
			}
			?>
		</center>
	</div>

</body>

</html>