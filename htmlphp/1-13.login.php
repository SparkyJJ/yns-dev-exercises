<?php
define('CSV_FILE', '1-13.users.csv');
session_start();
if (isset($_SESSION['access'])) {
    header('Location: 1-13.php');
    exit();
}

if (isset($_POST['register'])) {
    header('Location: 1-13.register.php');
} elseif (isset($_POST['login'])) {
    $username = htmlspecialchars($_POST['username']);
    $password = htmlspecialchars($_POST['password']);
    if (empty($username) || empty($password)) {
        $message = 'Please fill up the login fields.';
    } elseif (preg_match("/\s/", ($username . $password))) {
        $message = 'Whitespace is not allowed for username/password.';
    } elseif (strlen($password) < 8) { //Validates string length of password
        $message = 'Password should be at least 8 characters';
    } else {
        $handle = fopen(CSV_FILE, 'r');
        if (!$handle) {
            $message = 'Unable to connect.';
        } else {
            while (!feof($handle)) {
                $data = fgetcsv($handle);
                if ($data[0] == $username) {
                    if (password_verify($password, $data[1])) {
                        if (session_status() == PHP_SESSION_NONE) {
                            session_start();
                        }
                        $_SESSION['username'] = $username;
                        $_SESSION['password'] = $password;
                        $_SESSION['access'] = true;
                        fclose($handle);

                        header('Location: 1-13.php');
                        exit();
                    } else {
                        break;
                    }
                }
            }
            $message = 'Invalid username/password';
        }
        fclose($handle);
    }
}

?>
<html>

<head>
    <title>HTML & PHP 1-13</title>
</head>

<body>
    <div style="margin-top: 100px;">
        <table cellpadding="5" align="center">
            <thead>
                <th colspan="2">
                    <h2>Information System</h2>
                </th>
            </thead>
            <tbody>
                <?php
                if (isset($message)) {
                    echo "<tr><td colspan='2'>";
                    echo "<h3 style='color: red'>$message</h3>";
                    echo "</td></tr>";
                }
                ?>
                <form method="post">
                    <tr>
                        <td>
                            <label for="username"><b>Username: </b></label>
                        </td>
                        <td>
                            <input type="text" placeholder="Enter Username" name="username" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="password"><b>Password: </b></label>
                        </td>
                        <td>
                            <input type="password" placeholder="Enter Password" name="password" required>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button type="submit" name="login" style="width: 100%">Login</button>
                        </td>
                    </tr>
                </form>
                <form method="post">
                    <tr>
                        <td colspan="2">
                            <button type="submit" name="register" style="width: 100%">Register</button>
                        </td>
                    </tr>
                </form>
            </tbody>
        </table>
    </div>
</body>

</html>